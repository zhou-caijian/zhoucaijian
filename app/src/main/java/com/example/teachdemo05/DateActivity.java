package com.example.teachdemo05;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CalendarView;
import android.widget.Toast;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;

public class DateActivity extends AppCompatActivity {

    CalendarView calendarView;
    Button btnGetDate;
    int year,month,day;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_date);
        btnGetDate = findViewById(R.id.btnGetDate);
        calendarView = findViewById(R.id.calendarView);
        Calendar calendar = Calendar.getInstance();
        year = calendar.get(Calendar.YEAR);
        month = calendar.get(Calendar.MONTH) + 1;
        day = calendar.get(Calendar.DAY_OF_MONTH);

        calendarView.setOnDateChangeListener(new CalendarView.OnDateChangeListener() {
            @Override
            public void onSelectedDayChange(@NonNull CalendarView calendarView, int year, int month, int day) {
                //Toast.makeText(DateActivity.this, "" + year +"-" + (month + 1) + "-" + day , Toast.LENGTH_SHORT).show();
                DateActivity.this.year = year;
                DateActivity.this.month = month + 1;
                DateActivity.this.day = day;
            }
        });
        //做类型转换（长整型转日期时间）SimpleDateFormat;
        btnGetDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(DateActivity.this, "" + year + "年" + month + "月" + day + "日" , Toast.LENGTH_SHORT).show();
            }
        });

    }
}
