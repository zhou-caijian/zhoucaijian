package com.example.teachdemo05;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.util.Random;


public class ProgressBarActivity extends AppCompatActivity {

    ProgressBar progressBar;
    Button btnSet,btnStop;
    TextView tvMsg;
    int value = 5;
    boolean isTop = false;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_progress_bar);

        progressBar = findViewById(R.id.progressBar);
        btnSet = findViewById(R.id.btnSet);
        btnStop = findViewById(R.id.btnStop);
        tvMsg = findViewById(R.id.tvMsg);

        progressBar.setProgress(value);

        btnSet.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        while (!isTop){
                            Random random = new Random();

                            int intValue = random.nextInt(10);

                            value += intValue;

                            Log.v("random:" , ""+intValue + " value:" + value);
                            /*
                            if(value >= 100){
                                progressBar.setProgress(100);
                                tvMsg.setText("value:"+100);

                                break;
                            }*/
                            if(value >= 100){
                                value = 100;
                                isTop = true;
                            }
                            progressBar.setProgress(value);//老版本会报错
                            tvMsg.setText("value:"+value);//老版本会报错
                            try {
                                Thread.sleep(1000);
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                }).start();



            }
        });
        btnStop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                isTop = true;
            }
        });

    }
}
