package com.example.teachdemo05;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.Toast;

public class DatePickerActivity extends AppCompatActivity {

    Button btnGetDate;
    DatePicker datePicker;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_date_picker);
        btnGetDate = findViewById(R.id.btnGetDate);
        datePicker = findViewById(R.id.datePicker);

        datePicker.setOnDateChangedListener(new DatePicker.OnDateChangedListener() {
            @Override
            public void onDateChanged(DatePicker datePicker, int year, int month, int day) {
                Toast.makeText(DatePickerActivity.this, "" + year+"年" + (month + 1) +"月" + day+"日", Toast.LENGTH_SHORT).show();

            }
        });
        btnGetDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(DatePickerActivity.this, "" + datePicker.getYear()+"年" + (datePicker.getMonth() + 1) +"月" + datePicker.getDayOfMonth()+"日", Toast.LENGTH_SHORT).show();
            }
        });

    }
}
